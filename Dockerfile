FROM node:8-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install --production

# Bundle app source - copy Node application from the current directory into the WORKDIR
COPY . .

EXPOSE 80

# run the application using node
CMD [ "node", "app" ]
