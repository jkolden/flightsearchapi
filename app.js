const express = require('express');
const app     = express();

var unirest = require("unirest");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/", function (req, res) {
  const { origin1, destination1, departdate1 } = req.query;

  //call API
  var unireq = unirest("GET", "https://apidojo-kayak-v1.p.rapidapi.com/flights/create-session");

  unireq.query({
                 origin1,
                 destination1,
                 departdate1,
                 "cabin":    "e",
                 "currency": "USD",
                 "adults":   "1",
                 "bags":     "0"
               });

  unireq.headers({
                   "cache-control":   "no-cache",
                   "Connection":      "keep-alive",
                   "Accept-Encoding": "gzip, deflate",
                   "Host":            "apidojo-kayak-v1.p.rapidapi.com",
                   "Cache-Control":   "no-cache",
                   "Accept":          "*/*",
                   "X-RapidAPI-Key":  "93e5bd7e58mshbc7051b72ff05cdp1ffa98jsn0aa79536dca5",
                   "X-RapidAPI-Host": "apidojo-kayak-v1.p.rapidapi.com"
                 });

  unireq.end(function (unireponse) {
    if (unireponse.error) throw new Error(unireponse.error);

    //flight segment detail:
    const { segset, tripset, airlines, airports } = unireponse.body;

    if (!tripset || tripset.length === 0) {
      res.send([]);
      return;
    }

    let filteredList = tripset.filter(function (trip) {
      return Number(trip.maxstops) === 0;
    });

    const flights = [];

    for (i = 0; i < filteredList.length; i++) {
      let flightObject = {};
      //get the first segment for each trip:
      let segment      = segset[tripset[i].legs[0].segments[0]];

      //look up flight details from the flight segment object
      flightObject.equipmentType     = segment.equipmentType;
      flightObject.flightNumber      = segment.flightNumber;
      flightObject.originCode        = airports[segment.originCode];
      flightObject.airline           = airlines[segment.airlineCode];
      flightObject.leaveTimeDisplay  = segment.leaveTimeDisplay;
      flightObject.arriveTimeDisplay = segment.arriveTimeDisplay;
      flightObject.exactLowTotal     = tripset[i].exactLowTotal;

      flights.push(flightObject);
      flightObject = {};
    }

    return res.send(flights);
  });
});

const port = process.env.port || 3000;
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
