# Flight Search API
This is a proxy for calling the Kajak FlightSearch API. The result is a simple array that contains several pieces of data from the API response including flight time, lowest cost, departure time and arrival time. 

The url will accept three query parameters as follows:
- origin1
- destination1
- departdate1

## How to run this API
- clone this git repo to a local directory on your machine
- cd into the directory and run the following:
- npm install
- npm install express --save
- cd into the myapp directory and run:
- node app.js

The final API can then be accessed on your host machine as follows:
http://localhost:3000/?origin1=LAX&destination1=DFW&departdate1=2019-12-24

## Kajak flight API authentication
For now the API header contains a key, X-RapidAPI-Key, and the app.js hardcodes my key from rapidapi.com.

## Additional documentation
Additional documentation on the Kajak API can be found here: https://rapidapi.com/apidojo/api/kajak



